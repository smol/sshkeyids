package main

import (
	"errors"
	"fmt"
	"os"
	"strings"

	"golang.org/x/crypto/ssh"
)

var (
	errNoSSHAuthInfo     = errors.New("no SSH_AUTH_INFO_0 environment variable")
	errNoSSHConnection   = errors.New("no SSH_CONNECTION environment variable")
	errNoPAMUser         = errors.New("no PAM_USER environment variable")
	errNonPublicKeyLogin = errors.New("non-publickey ssh login")
)

// Stub to simplify environment-based testing.
var getenv = os.Getenv

func envGetSSHAuthInfo() (ssh.PublicKey, error) {
	sshAuthInfo := getenv("SSH_AUTH_INFO_0")
	if sshAuthInfo == "" {
		return nil, errNoSSHAuthInfo
	}
	if !strings.HasPrefix(sshAuthInfo, "publickey ") {
		// Other login mechanisms.
		return nil, errNonPublicKeyLogin
	}

	sshKey, _, _, _, err := ssh.ParseAuthorizedKey([]byte(sshAuthInfo[9:]))
	if err != nil {
		return nil, fmt.Errorf("could not parse the public key in SSH_AUTH_INFO_0: %w", err)
	}

	return sshKey, nil
}

func envGetUser() (string, error) {
	user := getenv("PAM_USER")
	if user == "" {
		return "", errNoPAMUser
	}
	return user, nil
}

func envGetRemoteIP() string {
	if s := getenv("PAM_RHOST"); s != "" {
		return s
	}
	if s := getenv("SSH_CONNECTION"); s != "" {
		return s[:strings.IndexByte(s, ' ')]
	}
	return ""
}
