package main

import (
	"fmt"
	"os"
	"os/user"
	"path/filepath"
	"testing"

	"golang.org/x/crypto/ssh"
)

var testAuthorizedKeys = `ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIETZSTNVfRH8yFxxTLJYppoLN3fZLUyJ+xfNbw+p1YId user2
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKHGU3+iLYiepGfenTlieDiv4hY4r0PlZa+mnb7NoKkc user1
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIB0aQ6m/foqJlc4xSb585ZvoJWiNlkn15Jm4445yiZky user3
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDA0pl2flnc4ubqV4F0oz2iKdavD+ISjSDe3BLmM5k/sRh7gwkVyDkjNIIKbV6l880pB/3Th2TOSOz+hHlb0CSx70aBe0FfGy+YaYEvJgo7PQp2MdVUFzpO1bc0B18zeUzCJwejB7EQHYitw/2ZzsfGkreyUSV+NBx/zpyVcAKDQL6J/IY1u2mt3l230LWkCo3vm6ROimj4Rv17EMGLO4/IWsj5Du2Dr702XPxx7bmVuPLbobtb9FVp6Ji9B0gLS0XEsovNMye5wXOvcVlSobXLgav7K1FdpTLAGe8CVgfl7Qo3kknza+RstmM8sWoSJb6/mhHZUZJKbdc2kfn2eLjBNivZ6G7mLaYpM/tH/4NycAFAnrAlGacnBKbgS9bPulT2dw0Uoi74PZVZRdtSv9G9GXMEsL3xPxb27rK6gsLV6IqpfwrFFqpTWzG4TSyMJu8T26a2STbcJWqAnscEks9iCeNYvzl2B0OF/SvotPncT86Zf2iSoOu+4HRrFzPdYfN1OKG/io012ekozcoAiqZmcDoRCpN0iJbm5KEXY7CY/xPk6/p9kYb8mZHROhtk/SgKygfSoBcg5WrKdjK5L9s0OpUyCDLzQCAIf/78+cJs+km83dE0264B0QAjTbtoStC6zSMf3wTQqPrmjnAURF/C6XlACLTip//KxdRCSkyMaw== user4
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICDtvYzY5G1bNBwiRTwvlWZtAvDHmwzRNVbDKyxy86pm user1
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIL4dNxeGoFa6WsOiJjYJNJlVowlBh3FhRRcVg5KQihMz user4
`

func withTestAuthorizedKeys(t *testing.T, ak string, f func(string)) {
	dir, err := os.MkdirTemp("", "")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(dir)

	kf, err := os.Create(filepath.Join(dir, "root"))
	if err != nil {
		t.Fatal(err)
	}
	fmt.Fprint(kf, ak) // nolint:errcheck
	kf.Close()
	path := kf.Name()

	*authorizedKeysFilePattern = dir + "/%u"

	f(path)
}

func TestAuthorizedKeys(t *testing.T) {
	withTestAuthorizedKeys(t, testAuthorizedKeys, func(path string) {
		for _, td := range []struct {
			key        string
			expectedID string
		}{
			{"ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICDtvYzY5G1bNBwiRTwvlWZtAvDHmwzRNVbDKyxy86pm", "user1"},
			{"ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICDtvYzY5G1bNBwiRTwvlWZtAvDHmwzRNVbDKyxy86pq", ""},
		} {
			key, _, _, _, err := ssh.ParseAuthorizedKey([]byte(td.key))
			if err != nil {
				t.Errorf("parse(%s) -> %v", td.key, err)
				continue
			}
			_, id, err := findKeyInAuthorizedKeys([]string{path}, key)
			if id != td.expectedID {
				t.Errorf("findKeyInAuthorizedkeys(%s): id=%s err=%v", td.key, id, err)
			}
		}
	})
}

func TestAuthorizedKeys_ExpandPaths(t *testing.T) {
	// Cheat by providing our own already "resolved" user.User.
	u := unixUser{
		name: "user",
		usr: &user.User{
			Uid:     "42",
			HomeDir: "/home/user",
		},
	}

	for _, td := range []struct {
		input, output string
	}{
		{"/keys/%u", "/keys/user"},
		{"/keys/%U", "/keys/42"},
		{"%h/.ssh/authorized_keys", "/home/user/.ssh/authorized_keys"},
		{".ssh/authorized_keys", "/home/user/.ssh/authorized_keys"},
		{"/keys/%u/authorized_keys", "/keys/user/authorized_keys"},
	} {
		o, err := expandSSHPaths(td.input, &u)
		if err != nil {
			t.Errorf("expandSSHPaths(%s): error: %v", td.input, err)
			continue
		}
		if len(o) != 1 {
			t.Errorf("expandSSHPaths(%s) returned more than one path: %+v", td.input, o)
			continue
		}
		if o[0] != td.output {
			t.Errorf("expandSSHPaths(%s) -> %s, expected %s", td.input, o[0], td.output)
		}
	}
}

func TestAuthorizedKeys_ExpandPaths_Fail(t *testing.T) {
	if _, err := expandSSHPaths("/etc/ssh/%Z", &unixUser{}); err == nil {
		t.Fatalf("expandSSHPaths did not fail on unknown token")
	}

	if _, err := expandSSHPaths("%h/.keys", &unixUser{name: "i-sincerely-hope-this-user-does-not-exist-on-your-system"}); err == nil {
		t.Fatalf("expandSSHPaths did not fail on unknown user")
	}
}
