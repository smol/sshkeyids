package main

import (
	"bytes"
	"errors"
	"flag"
	"fmt"
	"log"
	"os"
	"os/user"
	"path/filepath"
	"strings"

	"golang.org/x/crypto/ssh"
)

var (
	// Useful for testing.
	authorizedKeysFilePattern = flag.String("authorized-keys-file", "/etc/ssh/authorized_keys/%u", "file containing user authorized keys, a `pattern` following the same syntax as sshd_config AuthorizedKeysFile")
)

var errNotFound = errors.New("key not found")

// Lazy-load NSS information, avoid lookups unless necessary. Lazy
// loading makes the rest of the code quite ugly, due to the necessity
// for constant error checking, but it's worth it because in float's
// case we can get away with doing no NSS lookups at all.
type unixUser struct {
	name string
	usr  *user.User
}

func (u *unixUser) lookup() (err error) {
	u.usr, err = user.Lookup(u.name)
	return
}

func (u *unixUser) getUID() (string, error) {
	if u.usr == nil {
		if err := u.lookup(); err != nil {
			return "", err
		}
	}
	return u.usr.Uid, nil
}

func (u *unixUser) getHomedir() (string, error) {
	if u.usr == nil {
		if err := u.lookup(); err != nil {
			return "", err
		}
	}
	return u.usr.HomeDir, nil
}

// Replace tokens in an AuthorizedKeysFile directive. Mimics the SSH
// semantics, see section TOKENS in the sshd_config(5) manpage.
func expandSSHPaths(s string, u *unixUser) ([]string, error) {
	var out bytes.Buffer
	var esc bool
	for _, c := range s {
		if !esc {
			if c == '%' {
				esc = true
				continue
			}
			out.WriteRune(c)
		} else {
			switch c {
			case '%':
				out.WriteRune(c)
			case 'u':
				out.WriteString(u.name)
			case 'U':
				uid, err := u.getUID()
				if err != nil {
					return nil, fmt.Errorf("looking up uid: %w", err)
				}
				out.WriteString(uid)
			case 'h':
				homedir, err := u.getHomedir()
				if err != nil {
					return nil, fmt.Errorf("looking up homedir: %w", err)
				}
				out.WriteString(homedir)
			default:
				return nil, fmt.Errorf("unsupported token '%%%c'", c)
			}
			esc = false
		}
	}

	// Split the result on whitespace, AuthorizedKeysFile can
	// specify multiple paths.
	paths := strings.Fields(out.String())

	// If any of the paths are not absolute, they're relative to
	// the user's home directory.
	for i, path := range paths {
		if !strings.HasPrefix(path, "/") {
			homedir, err := u.getHomedir()
			if err != nil {
				return nil, fmt.Errorf("looking up homedir: %w", err)
			}
			paths[i] = filepath.Join(homedir, path)
		}
	}

	return paths, nil
}

// Return the local user's authorized_keys path.
func userAuthorizedKeysFile(username string) ([]string, error) {
	return expandSSHPaths(*authorizedKeysFilePattern, &unixUser{name: username})
}

// Find a specific SSH key in an authorized_keys file.
func findKeyInAuthorizedKeysFile(path string, marshaled []byte) (ssh.PublicKey, string, error) {
	authorizedKeysBytes, err := os.ReadFile(path)
	if err != nil {
		return nil, "", err
	}

	for len(authorizedKeysBytes) > 0 {
		pubKey, comment, _, rest, err := ssh.ParseAuthorizedKey(authorizedKeysBytes)
		if err != nil {
			log.Fatal(err)
		}

		if bytes.Equal(pubKey.Marshal(), marshaled) {
			return pubKey, comment, nil
		}
		authorizedKeysBytes = rest
	}

	return nil, "", errNotFound
}

// Find a specific SSH key in one of the possible authorized_keys files.
func findKeyInAuthorizedKeys(paths []string, sshKey ssh.PublicKey) (ssh.PublicKey, string, error) {
	marshaled := sshKey.Marshal()
	for _, path := range paths {
		k, c, err := findKeyInAuthorizedKeysFile(path, marshaled)
		if err == nil {
			return k, c, nil
		}
	}
	return nil, "", errNotFound
}
