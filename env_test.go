package main

import (
	"errors"
	"os"
	"testing"
)

func withTestEnv(env map[string]string, f func()) {
	getenv = func(key string) string {
		return env[key]
	}
	defer func() {
		getenv = os.Getenv
	}()

	f()
}

func TestEnv_GetSSHAuthInfo(t *testing.T) {
	withTestEnv(map[string]string{
		"SSH_AUTH_INFO_0": "publickey ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKHGU3+iLYiepGfenTlieDiv4hY4r0PlZa+mnb7NoKkc",
	}, func() {
		_, err := envGetSSHAuthInfo()
		if err != nil {
			t.Fatalf("envGetSSHAuthInfo: %v", err)
		}
	})
}

func TestEnv_GetSSHAuthInfo_NonPublicKey(t *testing.T) {
	withTestEnv(map[string]string{
		"SSH_AUTH_INFO_0": "password abcdef",
	}, func() {
		_, err := envGetSSHAuthInfo()
		if !errors.Is(err, errNonPublicKeyLogin) {
			t.Fatalf("envGetSSHAuthInfo: unexpected error %v", err)
		}
	})
}

func TestEnv_GetUser(t *testing.T) {
	withTestEnv(map[string]string{
		"PAM_USER": "user1",
	}, func() {
		u, err := envGetUser()
		if err != nil || u != "user1" {
			t.Fatalf("envGetUser: u=%v err=%v", u, err)
		}
	})
}

func TestEnv_GetRemoteIP(t *testing.T) {
	for _, env := range []map[string]string{
		{
			"SSH_CONNECTION": "127.0.0.1 10023",
		},
		{
			"PAM_RHOST": "127.0.0.1",
		},
	} {
		withTestEnv(env, func() {
			ip := envGetRemoteIP()
			if ip != "127.0.0.1" {
				t.Fatalf("unexpected ip for %v - %s", env, ip)
			}
		})
	}
}
