package main

import (
	"encoding/json"
	"errors"
	"flag"
	"log"
	"log/syslog"

	"golang.org/x/crypto/ssh"
)

const pamOpenSession = "open_session"

type record struct {
	User       string `json:"user"`
	LocalUser  string `json:"local_user"`
	Key        string `json:"key"`
	RemoteAddr string `json:"remote_addr,omitempty"`
}

var logger *log.Logger

func emit(rec record) {
	enc, err := json.Marshal(rec)
	if err != nil {
		logger.Printf("error serializing JSON object: %v", err)
		return
	}

	logger.Printf("@cee:%s", enc)
}

func handleOpenSession() error {
	sshKey, err := envGetSSHAuthInfo()
	if errors.Is(err, errNonPublicKeyLogin) {
		// User logged in without using a public key, do nothing.
		return nil
	} else if err != nil {
		return err
	}

	localUser, err := envGetUser()
	if err != nil {
		return err
	}

	keyPaths, err := userAuthorizedKeysFile(localUser)
	if err != nil {
		return err
	}

	_, user, err := findKeyInAuthorizedKeys(keyPaths, sshKey)
	if err != nil {
		return err
	}

	// Emit log entry.
	rec := record{
		User:      user,
		LocalUser: localUser,
		Key:       ssh.FingerprintSHA256(sshKey),
	}
	if addr := envGetRemoteIP(); addr != "" {
		rec.RemoteAddr = addr
	}

	emit(rec)

	return nil
}

func main() {
	log.SetFlags(0)
	flag.Parse()

	// Detect if we're (potentially) running as a PAM hook.
	if s := getenv("PAM_TYPE"); s == "" {
		log.Fatalf("error: must be invoked as pam_exec hook")
	}

	// Set up syslog, or our errors will get lost.
	slog, err := syslog.NewLogger(syslog.LOG_AUTH|syslog.LOG_INFO, 0)
	if err != nil {
		log.Fatalf("error: %v", err)
	}

	logger = slog

	// We only handle open_session events. For anything else, just
	// exit successfully.
	if s := getenv("PAM_TYPE"); s != pamOpenSession {
		return
	}

	if err := handleOpenSession(); err != nil {
		// Lack of SSH_CONNECTION env var means the hook is
		// being invoked for a non-ssh service, ignore it.
		if !errors.Is(err, errNoSSHConnection) {
			logger.Fatalf("error: %v", err)
		}
	}
}
