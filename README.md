# ssh-key-ids

*ssh-key-ids* is just a trivial post-login hook that logs (in syslog)
the owner of the SSH key used to log in.

Its intended use case is a situation where a single account (e.g.
root) is shared amongst multiple operators, with key-based
authentication. In this situation, a central authorized_keys file
should contain all the operator keys, each with a comment identifying
the operator.

The tool is implemented as a *pam_exec* hook, because only after
authentication has succeeded we can know which key was used to log in,
so installation is going to require some integration.

## Configuration

The tool needs to retrieve the specific SSH key, that was used for
authentication, from an authorized\_keys file in order to retrieve its
comment. It is important that the *--authorized-keys-file*
command-line option to ssh-key-wtmp matches the *AuthorizedKeysFile*
directive in your sshd\_config. Note that the default value for this
option works for the ai3/float environment, but it is not the SSH
default. For that, you'll need to set:

```
--authorized-keys-file=".ssh/authorized_keys .ssh/authorized_keys2"
```

You can use the same syntax as *AuthorizedKeysFile*, including token
substitution (see "man sshd_config").

### PAM

Something like the following, in */etc/pam.d/sshd*, should be
sufficient for system integration:

```
session [default=ignore] pam_exec.so quiet seteuid /usr/bin/ssh-key-wtmp
```

The Debian package will set this up automatically in
*/etc/pam.d/common-session*, which is slightly sub-optimal because it
will get invoked (harmlessly) from other services such as cron. You
might be better off manually editing */etc/pam.d/sshd* directly.

## History

This tool is a simplified version of
[ai3/tools/ssh-key-wtmp](https://git.autistici.org/ai3/tools/ssh-key-wtmp),
meant to be used in a more structured Kubernetes context with an
existing audit infrastructure, where the *wtmp* part of ssh-key-wtmp
is no longer necessary. By focusing exclusively on emitting audit
logs, we get rid of the local database and associated "session" code.
