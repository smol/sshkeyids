module git.autistici.org/smol/sshkeyids

go 1.21

require golang.org/x/crypto v0.22.0

require golang.org/x/sys v0.19.0 // indirect
